all: build push

V2_7=2.7
V2_7_MINOR=$(V2_7).2
V2_7_alpine=alpine3.13
V2_6=2.6
V2_6_MINOR=$(V2_6).6
V2_6_alpine=alpine3.13
V2_5=2.5
V2_5_MINOR=$(V2_5).8
V2_5_alpine=alpine3.13

# build and tag
build: build-2-7 build-2-6 build-2-5

# 2.7
build-2-7: build-2-7-alpine

build-2-7-alpine:
	docker build --tag erdalmutlu/ruby:$(V2_7_MINOR)-$(V2_7_alpine) - < Dockerfile-$(V2_7)-alpine
	# tag as $(V2_7)-$(V2_7_alpine)
	docker tag erdalmutlu/ruby:$(V2_7_MINOR)-$(V2_7_alpine) erdalmutlu/ruby:$(V2_7)-$(V2_7_alpine)
	# tag as $(V2_7)-alpine
	docker tag erdalmutlu/ruby:$(V2_7_MINOR)-$(V2_7_alpine) erdalmutlu/ruby:$(V2_7)-alpine
	# tag as $(V2_7)
	docker tag erdalmutlu/ruby:$(V2_7_MINOR)-$(V2_7_alpine) erdalmutlu/ruby:$(V2_7)
	# tag as latest
	docker tag erdalmutlu/ruby:$(V2_7_MINOR)-$(V2_7_alpine) erdalmutlu/ruby
# 2.6
build-2-6: build-2-6-alpine

build-2-6-alpine:
	docker build --tag erdalmutlu/ruby:$(V2_6_MINOR)-$(V2_6_alpine) - < Dockerfile-$(V2_6)-alpine
	# tag as $(V2_6)-alpine3.10
	docker tag erdalmutlu/ruby:$(V2_6_MINOR)-$(V2_6_alpine) erdalmutlu/ruby:$(V2_6)-$(V2_6_alpine)
	# tag as $(V2_6)-alpine
	docker tag erdalmutlu/ruby:$(V2_6_MINOR)-$(V2_6_alpine) erdalmutlu/ruby:$(V2_6)-alpine
	# tag as $(V2_6)
	docker tag erdalmutlu/ruby:$(V2_6_MINOR)-$(V2_6_alpine) erdalmutlu/ruby:$(V2_6)
# 2.5
build-2-5: build-2-5-alpine build-2-5-alpine3-9 build-2-5-alpine3-10

build-2-5-alpine:
	docker build --tag erdalmutlu/ruby:$(V2_5_MINOR)-$(V2_5_alpine) - < Dockerfile-$(V2_5)-alpine
	# tag as $(V2_5)-$(V2_5_alpine)
	docker tag erdalmutlu/ruby:$(V2_5_MINOR)-$(V2_5_alpine) erdalmutlu/ruby:$(V2_5)-$(V2_5_alpine)
	# tag as $(V2_5)-alpine
	docker tag erdalmutlu/ruby:$(V2_5_MINOR)-$(V2_5_alpine) erdalmutlu/ruby:$(V2_5)-alpine
	# tag as $(V2_5)
	docker tag erdalmutlu/ruby:$(V2_5_MINOR)-$(V2_5_alpine) erdalmutlu/ruby:$(V2_5)

build-2-5-alpine3-10:
	docker build --tag erdalmutlu/ruby:$(V2_5_MINOR)-$(V2_5_alpine) - < Dockerfile-$(V2_5)-alpine3.10
	# tag as $(V2_5)-alpine3.10
	docker tag erdalmutlu/ruby:$(V2_5_MINOR)-$(V2_5_alpine) erdalmutlu/ruby:$(V2_5)-alpine3.10
build-2-5-alpine3-9:
	docker build --tag erdalmutlu/ruby:$(V2_5_MINOR)-alpine3.9 - < Dockerfile-$(V2_5)-alpine3.9
	# tag as $(V2_5)-alpine3.9
	docker tag erdalmutlu/ruby:$(V2_5_MINOR)-alpine3.9 erdalmutlu/ruby:$(V2_5)-alpine3.9
#push to docker
push: push-2-7 push-2-6 push-2-5

push-2-7:
	docker push erdalmutlu/ruby:$(V2_7_MINOR)-$(V2_7_alpine)
	docker push erdalmutlu/ruby:$(V2_7)-$(V2_7_alpine)
	docker push erdalmutlu/ruby:$(V2_7)-alpine
	docker push erdalmutlu/ruby:$(V2_7)
	docker push erdalmutlu/ruby
push-2-6:
	docker push erdalmutlu/ruby:$(V2_6_MINOR)-$(V2_6_alpine)
	docker push erdalmutlu/ruby:$(V2_6)-$(V2_6_alpine)
	docker push erdalmutlu/ruby:$(V2_6)-alpine
	docker push erdalmutlu/ruby:$(V2_6)
push-2-5:
	docker push erdalmutlu/ruby:$(V2_5_MINOR)-$(V2_5_alpine)
	docker push erdalmutlu/ruby:$(V2_5)-$(V2_5_alpine)
	docker push erdalmutlu/ruby:$(V2_5)-alpine
	docker push erdalmutlu/ruby:$(V2_5)
	docker push erdalmutlu/ruby:$(V2_5_MINOR)-alpine3.9
	docker push erdalmutlu/ruby:$(V2_5)-alpine3.9
