# docker-ruby

Supported tags and respective Dockerfile links

2.7.2-alpine3.13, 2.7-alpine3.13, 2.7-alpine, 2.7, latest

2.6.6-alpine3.10, 2.6-alpine3.10, 2.6-alpine, 2.6

2.5.7-alpine3.10, 2.5-alpine3.10, 2.5-alpine, 2.5

2.5.7-alpine3.9, 2.5-alpine3.9
